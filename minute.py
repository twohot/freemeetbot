"""Classes for Notetaking"""

# import time


TMPL = {
    "title": "Minutes of Special Meeting of the {0} held on {1}",
    "agreed": "",
}

# Todo: Pick Schedule from Agenda in the future
SCHED = {
    "date": "25-04-2020 18:30:00",
    "duration": 30,  # in seconds
}

TYPES = [
    "ACTION",
    "ADJOURNMENT",
    "CONCLUSION",
    "HEADING",
    "IDEA",
    "INFO",
    "LINK",
    "PLAIN",
    "POLL",
    "TIME",
]


DATAKEYS = ["id", "text", "type", "group", "date", "contributor", "assigned"]


class Note:
    """A Minute Note"""

    def __new__(cls, **kwargs):
        """Create an instance of Note only when critical keys exist"""
        if ("id", "text") not in kwargs:
            raise KeyError("Notes must have Id and content")
        return super(Note, cls).__new__(cls, kwargs)

    def __init__(self, **kwargs):
        """Initialize the note"""
        self.__data = dict.fromkeys(DATAKEYS, value=None)
        for key, value in kwargs.items():
            if key in DATAKEYS:
                if key == "type" and value not in TYPES:
                    # default to PLAIN text if value not compliant
                    value = "PLAIN"
                self.__data[key] = value

    def text(self,):
        """Return the note"""
        return self.__data["text"]

    def uid(self,):
        """Return the unique id of Note"""
        return self.__data["id"]

    def update(self, value):
        """Update/change the note"""
        self.__data["text"] = value


class Group:
    """A group holding a meeting"""

    def __init__(self, value):
        """Initialize Group"""
        self.__name = value
        self.__present = list()

    def rename(self, value):
        """Give the group a new name"""
        self.__name = value

    def name(self,):
        """Return the name of the Group"""
        return self.__name


class Title(Note):
    """Title of a Minute Document"""

    def __init__(self, uid, template=None, group=None, day=None):
        """Initialize the Title"""
        if not template:
            value = TMPL["title"].format(group, day)
        super().__init__(uid, value)
        self.__group = Group(group)
        self.__day = day
        self.type = "title"

    def group(self,):
        """Return the group in the Title"""
        return self.__group.name()

    def rename_group(self, value=None):
        """Change the name of group in the Title"""
        self.__group.rename(value)

    def day(self,):
        """Return the day of Title"""
        return self.__day

