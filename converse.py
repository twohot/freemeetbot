"""Commands for conversing with Freemeetbot"""

import time
import minute

# meeting entries
# { group chat_id : meeting object }
__MEETINGS = {}


def chair(update):
    """Make the user with id in the group the Chair"""
    chairperson = update["username"]
    message = "{} is set to Chair this meeting".format(chairperson)
    return (message, update["chat_id"])


def chat(update):
    """Start a private chat with bot"""
    message = "Hi, How may I be of service to you today?"
    return (message, update["chat_id"])


def end(update):
    """Stop a meeting in the group with associated id"""
    # remove the instance from __MEETINGS too
    gmtdate = time.gmtime(update["date"])
    endtime = time.strftime("%A, %B%e, at %r %Z", gmtdate)
    message = "Meeting ended on {}".format(endtime)
    return (message, update["chat_id"])


def start(update):
    """Start a meeting"""
    # add an instance of meeting into __MEETINGS.
    gmtdate = time.gmtime(update["date"])
    starttime = time.strftime("%A, %B%e, at %r %Z", gmtdate)
    message = "Meeting started on {}".format(starttime)
    return (message, update["chat_id"])


def topic(update):
    """Set the current topic"""
    # modify instance in __MEETINGS
    # pin the topic
    message = "Current topic is now {}".format("WHATEVER")
    return (message, update["chat_id"])



class Task:
    """Something that needs to be done within a specified period"""

    def __init__(self, task):
        """initialize task object from a dict"""
        self.__task = {
            "id": None,
            "note": None,
            "owner": None,
            "users": None,
            "start": None,
            "end": None,
        }
        self.init(task)

    def init(self, task):
        """Initialize/reinitialize task Object from task dictionary"""
        if not ("id", "note" in task.keys())[1]:
            # A Task must have an id and text(note)
            print("ERROR:\tCannot create Task from argument")
            return
        for key in task.keys():
            if key in self.__task:
                self.__task[key] = task[key]

    def info(self):
        """Print task to Stdout"""
        msg = (
            "Task ID: {0}\n\t\tTask-{0} is assigned to: {1}\n\t\t"
            + "Task-{0} Starts: {2}\n\t\tTask-{0} Ends: {3}\n\t\t"
            + "Task-{0} Description: {4}"
        )
        print(
            msg.format(
                self.__task["id"],
                self.__task["note"],
                self.__task["owner"],
                self.__task["users"],
                self.__task["start"],
                self.__task["end"],
            )
        )




class Minutes:
    """Sections of a Meeting Minutes"""

    def __init__(self,):
        """initialize Sections"""
        # doc_title
        # meeting_title
        # venue
        # regrets
        # moderators
        # agendas
        # closing
        # attendance
        pass


class Agenda:
    """A meeting Agenda"""

    def __init__(self, title):
        """Initialize the Agenda"""
        self.__title = title
        self.__conclusions = dict()
        self.__actions = dict()
        self.__categories = dict()

    def add(self, category, values):
        """add a conclusion or action"""
        try:
            self.__categories[category][values["id"]] = Task(values)
        except KeyError:
            print("ERROR:\tTried adding new category with bad Key")

    def remove(self, category, key):
        """Remove an Agenda"""
        pass


class Meeting:
    """A meeting"""

    def __init__(self, args):
        """Initialize a meeting"""
        self.agenda = dict()

    def active(self, arg):
        """Returns the current/active agenda"""
        pass


IMPL = {
    "start": chat,
    "start@freemeetbot": start,
    "end": end,
    "end@freemeetbot": end,
    "chair": chair,
    "chair@freemeetbot": chair,
    "topic": topic,
    "topic@freemeetbot": topic,
}
