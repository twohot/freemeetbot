"""Free MeetBot for Telegram Main Module"""

import os
import json
import time
import urllib
import requests
import converse


def flatten_update(update):
    """Converts Telegram's json_dict to a flat dict"""
    # comfirm that update is not inline-query
    flat = dict()
    update_t = None
    if "inline_query" in update:
        update_t = "inline_query"
        flat.update(
            [
                ("chat_id", "",),
                ("chat_group", "",),
                ("chat_type", "",),
                ("date", "",),
                ("message", update["query"],),
            ]
        )
    else:
        update_t = "message"
        flat.update(
            [
                ("chat_group", "",),
                ("chat_type", update[update_t]["chat"]["type"],),
                ("chat_id", update[update_t]["chat"]["id"],),
                ("date", update[update_t]["date"],),
                ("message", update[update_t]["text"],),
            ]
        )
        if flat["chat_type"] == "group":
            flat.update([("chat_group", update[update_t]["chat"]["title"],)])
    flat.update(
        [
            ("update_type", update_t,),
            ("sender_id", update[update_t]["from"]["id"],),
            ("first_name", update[update_t]["from"]["first_name"],),
            ("last_name", update[update_t]["from"]["last_name"],),
            ("username", update[update_t]["from"]["username"],),
            ("is_bot", update[update_t]["from"]["is_bot"],),
        ]
    )
    return flat


def get_url(url):
    """Send a http request with url"""
    response = requests.get(url)
    content = response.content.decode("utf8")
    # print(content)
    return content


def get_json_from_url(url):
    """Convert object to dict"""
    content = get_url(url)
    js_dict = json.loads(content)
    return js_dict


def get_updates(update_id=None):
    """Get updates from Bot"""
    url = URL + "getUpdates?timeout=100"
    if update_id:
        url += "&offset={}".format(update_id)
    js_dict = get_json_from_url(url)
    return js_dict


def get_last_update_id(updates):
    """Get id of last update"""
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)


def handle_updates(updates):
    """Process the captured updates"""
    for update in updates["result"]:
        try:
            update = flatten_update(update)
            print(update)
            texts = update["message"].split()
            if texts[0].startswith("/"):
                response = converse.IMPL.get(texts[0].strip("/"))(update)
            else:
                # respond to ordinary chat message
                response = (
                    "",
                    None,
                )
            send_message(*response)
        except (KeyError, TypeError):
            print("Someone requested something that wasn't implemented")


def send_message(text, chat_id):
    """Send a message through the Bot to relevant recepient"""
    text = urllib.parse.quote_plus(text)
    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
    get_url(url)


def get_config(path=None):
    """Get User's Bot configuration"""
    if not path:
        path = os.environ.get("HOME") + "/.config/freemeetbot/config"
        print(path)
    try:
        config = dict()
        with open(path, "r") as cfg:
            for setting in list(cfg):
                key, value = setting.strip().split("=")
                config[key] = value
        return config
    except (KeyError, PermissionError, FileNotFoundError):
        print("faulty or non-existent configuration file")


class Bot:
    """the Virtual Assistant"""

    def __init__(self, arg):
        """Initialize Bot with TOKEN"""
        self.__cfg = dict()
        self.load(arg)

    def load(self, path=None):
        """Load settings from path"""
        self.__cfg = get_config(path)

    def token(self):
        """Return bot's token"""
        return self.__cfg["id"]


BOT = Bot(None)
URL = "https://api.telegram.org/bot{}/".format(BOT.token())


def main():
    """Run Bot"""
    last_update_id = None
    while True:
        updates = get_updates(last_update_id)
        print(updates)
        if len(updates["result"]) > 0:
            last_update_id = get_last_update_id(updates) + 1
            handle_updates(updates)
        time.sleep(0.5)


if __name__ == "__main__":
    main()
